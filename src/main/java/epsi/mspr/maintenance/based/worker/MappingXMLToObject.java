package epsi.mspr.maintenance.based.worker;

import epsi.mspr.maintenance.based.models.Country;
import epsi.mspr.maintenance.based.models.Document;

import javax.print.Doc;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class MappingXMLToObject {



    public MappingXMLToObject() throws JAXBException {
    }


    public static Document xmlToDocument(String pathname) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Document.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        File source = new File(pathname);
        Document newDocument = (Document) jaxbUnmarshaller.unmarshal(source);
        return newDocument;
    }
}
