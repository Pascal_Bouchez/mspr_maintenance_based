package epsi.mspr.maintenance.based.models;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

//@Root(name = "Document")
@XmlRootElement(name = "Document")
public class Document {

    private String url;

    private Country country;

    private List<Arrondissement> arrondissementList;

    public Document() {
    }

    public Document(String xmnls, Country country) {
        this.url = xmnls;
        this.country = country;
    }

    public Document(String url, Country country, List<Arrondissement> arrondissementList) {
        this.url = url;
        this.country = country;
        this.arrondissementList = arrondissementList;
    }

    @XmlAttribute(name = "xmlns:xsi")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlElement(name = "Ctry")
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @XmlElement(name = "Arrdt")
    public List<Arrondissement> getArrondissementList() {
        return arrondissementList;
    }

    public void setArrondissementList(List<Arrondissement> arrondissementList) {
        this.arrondissementList = arrondissementList;
    }

    @Override
    public String toString() {
        return "Document{" +
                "url='" + url + '\'' +
                ", country=" + country +
                ", arrondissementList=" + arrondissementList +
                '}';
    }
}
