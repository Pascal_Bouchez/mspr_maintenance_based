package epsi.mspr.maintenance.based.models;

import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

@Table(name = "plage")
public class Plage {

    private String Fn;

    private String Db;

    public Plage() {
    }

    public Plage(String fn, String db) {
        Fn = fn;
        Db = db;
    }

    @XmlAttribute(name = "Fn")
    public String getFn() {
        return Fn;
    }

    public void setFn(String fn) {
        Fn = fn;
    }

    @XmlAttribute(name = "Db")
    public String getDb() {
        return Db;
    }

    public void setDb(String db) {
        Db = db;
    }

    @Override
    public String toString() {
        return "Plage{" +
                "Fn='" + Fn + '\'' +
                ", Db='" + Db + '\'' +
                '}';
    }
}
