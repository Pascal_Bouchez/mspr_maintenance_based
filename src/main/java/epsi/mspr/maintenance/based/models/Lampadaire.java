package epsi.mspr.maintenance.based.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@Table(name = "lampadaire")
public class Lampadaire implements Serializable {

    @Id
    @Column(name = "idLampadaire", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idLampadaire;

    @NotNull
    @Column(name = "id")
    private String Id;

    @NotNull
    @Column(name = "ltd")
    private String Ltd;

    @NotNull
    @Column(name = "lgtd")
    private String Lgtd;

    public Lampadaire() {
    }

    public Lampadaire(String id, String ltd, String lgtd) {
        Id = id;
        Ltd = ltd;
        Lgtd = lgtd;
    }

    @XmlElement(name = "Id")
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @XmlElement(name = "Ltd")
    public String getLtd() {
        return Ltd;
    }

    public void setLtd(String ltd) {
        Ltd = ltd;
    }

    @XmlElement(name = "Lgtd")
    public String getLgtd() {
        return Lgtd;
    }

    public void setLgtd(String lgtd) {
        Lgtd = lgtd;
    }

    @Override
    public String toString() {
        return "Lampadaire{" +
                "Id='" + Id + '\'' +
                ", Ltd='" + Ltd + '\'' +
                ", Lgtd='" + Lgtd + '\'' +
                '}';
    }

}
