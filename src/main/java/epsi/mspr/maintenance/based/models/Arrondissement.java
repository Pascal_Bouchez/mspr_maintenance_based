package epsi.mspr.maintenance.based.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

@Table(name = "arrondissement")
public class Arrondissement implements Serializable {

    @Id
    @Column(name = "idArrdt", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idArrdt;

    @Column(name = "Id")
    private String Id;

    @NotNull
    @Column(name = "code")
    private String Code;

    @OneToMany(mappedBy = "arrondissement", targetEntity = Lampadaire.class)
    private List<Lampadaire> lampadaires;

    @OneToMany(mappedBy = "arrondissement", targetEntity = Plage.class)
    private List<Plage> plages;

    public Arrondissement() {
    }

    public Arrondissement(List<Lampadaire> lampadaires, String code) {
        this.lampadaires = lampadaires;
        Code = code;
    }

    public Arrondissement(String code, List<Lampadaire> lampadaires, List<Plage> plages) {
        Code = code;
        this.lampadaires = lampadaires;
        this.plages = plages;
    }

    public Arrondissement(String id, String code, List<Lampadaire> lampadaires, List<Plage> plages) {
        Id = id;
        Code = code;
        this.lampadaires = lampadaires;
        this.plages = plages;
    }

    @XmlElement(name = "Lpdre")
    public List<Lampadaire> getLampadaires() {
        return lampadaires;
    }

    public void setLampadaires(List<Lampadaire> lampadaires) {
        this.lampadaires = lampadaires;
    }

    @XmlAttribute(name = "Code")
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }


    @XmlElement(name = "Plge")
    public List<Plage> getPlages() {
        return plages;
    }

    public void setPlages(List<Plage> plages) {
        this.plages = plages;
    }

    @XmlAttribute(name = "Id")
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "Arrondissement{" +
                "idArrdt=" + idArrdt +
                ", Id='" + Id + '\'' +
                ", Code='" + Code + '\'' +
                ", lampadaires=" + lampadaires +
                ", plages=" + plages +
                '}';
    }
}
