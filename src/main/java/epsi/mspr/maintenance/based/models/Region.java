package epsi.mspr.maintenance.based.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

@Table(name = "region")
public class Region implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "code")
    private String Code;

    @OneToMany(mappedBy = "region", targetEntity = Departement.class)
    private List<Departement> departements;

    public Region() {
    }

    public Region(String code, List<Departement> departements) {
        Code = code;
        departements = departements;
    }

    @XmlAttribute(name = "Code")
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    @XmlElement(name = "Dpt")
    public List<Departement> getDepartements() {
        return departements;
    }

    public void setDepartements(List<Departement> departements) {
        this.departements = departements;
    }

    @Override
    public String toString() {
        return "Region{" +
                "Code='" + Code + '\'' +
                ", departements=" + departements +
                '}';
    }
}
