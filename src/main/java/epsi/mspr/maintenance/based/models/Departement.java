package epsi.mspr.maintenance.based.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

@Table(name = "departement")
public class Departement implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "code")
    private String Code;

    @OneToMany(mappedBy = "departement", targetEntity = Arrondissement.class)
    private List<Arrondissement> arrondissements;

    public Departement() {
    }

    public Departement(String code, List<Arrondissement> arrondissements) {
        Code = code;
        arrondissements = arrondissements;
    }

    @XmlAttribute(name = "Code")
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    @XmlElement(name = "Arrdt")
    public List<Arrondissement> getArrondissements() {
        return arrondissements;
    }

    public void setArrondissements(List<Arrondissement> arrondissements) {
        this.arrondissements = arrondissements;
    }

    @Override
    public String toString() {
        return "Departement{" +
                "Code='" + Code + '\'' +
                ", arrondissements=" + arrondissements +
                '}';
    }
}
