package epsi.mspr.maintenance.based.models;
import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

@Table(name = "country", catalog = "mspr_maintenance_BASED")
public class Country implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "code", length = 30, nullable = false)
    @NotNull
    private String Code;

    @OneToMany(mappedBy = "country", targetEntity = Region.class)
    private List<Region> regions;

    public Country() {
    }

    public Country(String code, List<Region> regions) {
        Code = code;
        this.regions = regions;
    }

    @XmlAttribute(name = "Code")
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    @XmlElement(name = "Rg")
    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    @Override
    public String toString() {
        return "Country{" +
                "Code='" + Code + '\'' +
                ", regions=" + regions +
                '}';
    }


}
