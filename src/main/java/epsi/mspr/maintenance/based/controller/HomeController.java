package epsi.mspr.maintenance.based.controller;

import epsi.mspr.maintenance.based.configuration.AbstractController;
import epsi.mspr.maintenance.based.models.*;
import epsi.mspr.maintenance.based.worker.MappingXMLToObject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
public class HomeController extends AbstractController<Resource> {

    private Logger logger = LogManager.getLogger(HomeController.class);


    @GetMapping(value = "/eclairage/horaires/{date}")
    public ResponseEntity<InputStreamResource> getPublicLightingInformationForDay(HttpServletRequest request, HttpServletResponse response, @PathVariable String date) throws JAXBException {

        /*Pattern pattern = Pattern.compile("/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/");
        Matcher matcher = pattern.matcher(date);
        if (matcher.matches()==true)
        {*/

        Document plagesHoraires = new Document();
        Document emplacementsLampadaires = new Document();
        if (isDateValid(date.toString())) {

            try {
                plagesHoraires = MappingXMLToObject.xmlToDocument("C:/Users/Pascal/Desktop/École/4ème année/MSPR Maintenance Applicative/Existant/BASED/" + date.toString() + ".xml");
                emplacementsLampadaires = MappingXMLToObject.xmlToDocument("C:/Users/Pascal/Desktop/École/4ème année/MSPR Maintenance Applicative/Existant/BASED/lclEclr.xml");
            } catch (IllegalArgumentException exception) {
                System.out.println("La date entrée ne correspond pas à un jour existant dans le systeme pour le moment");
            }

            String[] csvHeader = {
                    "latitude", "longitude", "extinction", "allumage"
            };

            ByteArrayInputStream byteArrayOutputStream;

            try (
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    // defining the CSV printer
                    CSVPrinter csvPrinter = new CSVPrinter(
                            new PrintWriter(out),
                            // withHeader is optional
                            CSVFormat.DEFAULT.withHeader(csvHeader)
                    );
            ) {
                // populating the CSV content
                for (List<String> record : returnLocationAndOnOffLighting(plagesHoraires, emplacementsLampadaires))
                    csvPrinter.printRecord(record);

                // writing the underlying stream
                csvPrinter.flush();

                byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }

            InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

            String csvFileName = "msprDemo.csv";

            // setting HTTP headers
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
            // defining the custom Content-Type
            headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");

            return new ResponseEntity<InputStreamResource>(
                    fileInputStream,
                    headers,
                    HttpStatus.OK
            );

        } else {
            return new ResponseEntity<InputStreamResource>(null, getHeaders(), HttpStatus.NOT_FOUND);
        }
    }

    final static String DATE_FORMAT = "dd-MM-yyyy";

    public static boolean isDateValid(String date)
    {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public List<List<String>> returnLocationAndOnOffLighting(Document plagesHoraires, Document listeLampadaires){
        List<List<String>> toConvertInCsv = new ArrayList<>();

        for (Arrondissement oneArrondissement: listeLampadaires.getCountry().getRegions().get(0).getDepartements().get(0).getArrondissements()) {
            for (Arrondissement anotherArrondissement: plagesHoraires.getArrondissementList()){
                if (oneArrondissement.getCode().equals(anotherArrondissement.getId())){
                    if (oneArrondissement.getLampadaires() != null){
                        for (Lampadaire oneLampadaire: oneArrondissement.getLampadaires()){
                            for (Plage onePlage: anotherArrondissement.getPlages()){
                                toConvertInCsv.add(Arrays.asList(oneLampadaire.getLtd(),oneLampadaire.getLgtd(),onePlage.getFn(), onePlage.getDb()));
                            }
                        }
                    }

                }
            }
        }

        return toConvertInCsv;
    }

}
