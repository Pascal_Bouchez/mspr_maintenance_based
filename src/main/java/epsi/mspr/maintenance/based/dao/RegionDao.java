package epsi.mspr.maintenance.based.dao;

import epsi.mspr.maintenance.based.models.Region;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionDao extends CrudRepository<Region,Integer> {
}
