package epsi.mspr.maintenance.based.dao;

import epsi.mspr.maintenance.based.models.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryDao extends CrudRepository<Country,Integer> {
}
