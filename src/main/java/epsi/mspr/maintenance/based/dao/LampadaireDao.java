package epsi.mspr.maintenance.based.dao;

import epsi.mspr.maintenance.based.models.Lampadaire;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LampadaireDao extends CrudRepository<Lampadaire,Integer> {
}
