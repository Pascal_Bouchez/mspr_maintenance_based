package epsi.mspr.maintenance.based.dao;

import epsi.mspr.maintenance.based.models.Arrondissement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArrondissementDao extends CrudRepository<Arrondissement,Integer> {
}
