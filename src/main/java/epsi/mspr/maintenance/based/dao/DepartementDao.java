package epsi.mspr.maintenance.based.dao;

import epsi.mspr.maintenance.based.models.Departement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartementDao extends CrudRepository<Departement,Integer> {
}
